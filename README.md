# README #

Steps required for running the application.

### What is this repository for? ###

* Migration of IMAP accounts
* Version 1.1

### How do I get set up? ###

* Copy accounts.sample.txt and rename to accounts.txt
* Update the line with your old account and new account details using one line per account

*oldusername1;oldpassword1;newusername1;newpassword1*

* Add the hostname in the file syncmail.sh
* Open Docker Toolbox
* Change directory to the docker-imap folder
* Change permissions to 777 for all files and folders
* Deployment instructions

*./run.sh*
### Who do I talk to? ###

* Craig as programmer
* Dave as previous user