FROM skempken/imapsync
MAINTAINER Craig Vincent <craig@accentdesign.co.uk>
ADD . /imap
ENTRYPOINT ["/imap/syncmail.sh"]
